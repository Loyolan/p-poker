# LAUNCH
python -m venv env


(linux): source env/bin/activate
(win): env\Scripts\activate


pip install -r requirements.txt


python launch.py