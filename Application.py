"""
Example script for testing the Azure ttk theme
Author: rdbende
License: MIT license
Source: https://github.com/rdbende/ttk-widget-factory
"""


import tkinter as tk
from controllers.MenuController import MenuController
from models.Game import Game
import uuid

class Application(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)
        self.title("")
        self.tk.call("source", "azure.tcl")
        self.tk.call("set_theme", "dark")
        self.game = Game(id=uuid.uuid4())
        # Set a minsize for the window, and place it in the middle
        self.attributes('-fullscreen', True)
        self.update()
        # self.minsize(self.winfo_width(), self.winfo_height())
        # x_coordinate = int((self.winfo_screenwidth() / 2) - (self.winfo_width() / 2))
        # y_coordinate = int((self.winfo_screenheight() / 2) - (self.winfo_height() / 2))
        # self.geometry("+{}+{}".format(x_coordinate, y_coordinate - 20))

        # Create controllers
        self.menu_controller = MenuController(self, self.game)

        # Show the default controller
        self.show_controller(self.menu_controller)

    def show_controller(self, controller):
        # Destroy the current controller
        if hasattr(self, "current_controller"):
            self.current_controller.pack_forget()

        # Show the new controller
        if controller == self.menu_controller:
            game = Game(uuid.uuid4())
            controller = MenuController(self, game)
        controller.pack(fill="both", expand=True)
        self.current_controller = controller

    def close_all(self):
        self.destroy()