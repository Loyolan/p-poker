from tkinter import ttk
import tkinter as tk
from models.Player import Player
from models.Game import Game
import uuid
from controllers.GameController import GameController

class MenuController(ttk.Frame):
    def __init__(self, parent, game):
        ttk.Frame.__init__(self)
        self.parent = parent
        self.game = game
        # Make the app responsive
        for index in [0, 1, 2]:
            self.columnconfigure(index=index, weight=1)
            self.rowconfigure(index=index, weight=1)
        
        # Create widgets :)
        self.setup_widgets()

    def setup_widgets(self):
        # Label
        self.label = ttk.Label(
            self,
            text=f"PLANNING POKER",
            justify="center",
            font=("Arial", 30, "bold"),
        )
        self.label.place(relx=0.5, rely=0.1, anchor="center")

        # Create a Frame for the Checkbuttons
        self.check_frame = ttk.LabelFrame(self, text="START NEW GAME", padding=(100, 20))
        self.check_frame.grid(
            row=0, column=0, padx=(20, 10), pady=(20, 10), sticky="nsew"
        )
        self.check_frame.place(relx=0.5, rely=0.5, anchor="center")

        # GAME NAME
        self.lbl_name = ttk.Label(
            self.check_frame,
            text="Number of players",
            justify="center",
            font=("-size", 8, "-weight", "normal"),
        )

        self.lbl_name.grid(row=0, column=0, padx=5, pady=5, sticky="nsew")

        self.nb_players = ttk.Spinbox(self.check_frame, from_=2, to=8, increment=1)
        self.nb_players.insert(0, "2")
        self.nb_players.grid(row=1, column=0, padx=5, pady=5, sticky="nsew")

        # PLAYER NAME
        self.lbl_pname = ttk.Label(
            self.check_frame,
            text="Your display name",
            justify="center",
            font=("-size", 8, "-weight", "normal"),
        )

        self.lbl_pname.grid(row=2, column=0, padx=5, pady=5, sticky="nsew")

        self.entry_pname = ttk.Entry(self.check_frame, width=50, )
        self.entry_pname.insert(0, "")
        self.entry_pname.grid(row=3, column=0, padx=5, pady=5, sticky="nsew")
        
        # SWITCH
        self.switch_toggle_online = ttk.Checkbutton(
            self.check_frame, text="Online Game", style="Switch.TCheckbutton", command=self.toogle_online
        )
        self.switch_toggle_online.grid(row=4, column=0, padx=5, pady=5, sticky="nsew")

        # LINK
        self.lbl_link = ttk.Label(
            self.check_frame,
            text="Invitation link",
            justify="center",
            font=("-size", 8, "-weight", "normal"),
        )

        self.lbl_link.grid(row=5, column=0, padx=5, pady=5, sticky="nsew")

        self.entry_link = ttk.Entry(self.check_frame, width=50, )
        self.entry_link.insert(0, f"https://ppoker.com/play/{self.game.id}")
        self.entry_link.config(state="readonly")
        self.entry_link.grid(row=6, column=0, padx=5, pady=5, sticky="nsew")

        self.lbl_link.grid_remove()
        self.entry_link.grid_remove()

        # SUBMIT
        self.accentbutton = ttk.Button(
            self.check_frame, text="Start Game", style="Accent.TButton", command=self.start_game
        )
        self.accentbutton.grid(row=7, column=0, padx=5, pady=10, sticky="nsew")

        # ERROR
        self.login_error = ttk.Label(
            self.check_frame,
            text="You must have a display name",
            justify="center",
            font=("Arial", 10, "normal"),
            foreground="#DC3545"
        )
        self.login_error.grid(row=8, column=0, padx=5, pady=5, sticky="nsew")
        self.login_error.grid_remove()

        # QUIT
        self.btnQuit = ttk.Button(
            self, text="Quit", command=self.close
        )

        self.btnQuit.place(relx=0.95, rely=0.9, anchor="center")

    def toogle_online(self):
        if "selected" in self.switch_toggle_online.state():
            self.lbl_link.grid()
            self.entry_link.grid()
        else:
            self.lbl_link.grid_remove()
            self.entry_link.grid_remove()

    def reset_form(self):
        self.entry_pname.delete(0, tk.END)
        self.entry_pname.insert(0, "")
        self.entry_link.delete(0, tk.END)
        self.entry_link.insert(0, "")
        self.switch_toggle_online.invoke()

    def start_game(self):
        nb_players = self.nb_players.get()
        player = self.entry_pname.get()
        owner = Player(name=player)

        if(player != ''):
            players = []
            players.append(owner)
            self.game.owner = owner
            self.game.players = players
            self.game.nb_players = nb_players
            
            self.reset_form()
            game_controller = GameController(self.parent, self.game)
            self.parent.show_controller(game_controller)
        else:
            self.login_error.grid()
            self.after(10000, self.hide_login_error())

    def hide_login_error(self):
        self.login_error.place_forget()

    def close(self):
        self.parent.close_all()