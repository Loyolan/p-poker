from tkinter import ttk

class GameController(ttk.Frame):
    def __init__(self, parent, game):
        ttk.Frame.__init__(self)

        # Make the app responsive
        for index in [0, 1, 2]:
            self.columnconfigure(index=index, weight=1)
            self.rowconfigure(index=index, weight=1)

        self.parent = parent
        self.game = game
        # Create widgets
        self.setup_widgets()

    def setup_widgets(self):
        # Label
        self.label = ttk.Label(
            self,
            text=f"PLANNING POKER GAME ({self.game.nb_players})",
            justify="center",
            font=("Arial", 30, "bold"),
        )
        self.label.place(relx=0.5, rely=0.09, anchor="center")

        # SUBMIT
        self.btnInvite = ttk.Button(
            self, text="Copy inivitation link", style="Accent.TButton", command=self.invite_player
        )

        self.invite_success = ttk.Label(
            self,
            text="Invitation link copied !",
            justify="center",
            font=("Arial", 10, "normal"),
            foreground="#106C50"
        )
    
        self.btnInvite.place(relx=0.88, rely=0.1, anchor="center")
        self.invite_success.place_forget()
        
        # NEW GAME
        self.btnNewGame = ttk.Button(
            self, text="New Game", command=self.new_game
        )

        style = ttk.Style()

        self.btnNewGame.place(relx=0.95, rely=0.1, anchor="center")

        # QUIT
        self.btnQuit = ttk.Button(
            self, text="Quit", command=self.close
        )

        self.btnQuit.place(relx=0.95, rely=0.9, anchor="center")

        # Panedwindow
        self.paned = ttk.PanedWindow(self, width=900, height=400)
        self.paned.grid(row=3, column=3, pady=(25, 5), sticky="nsew")

        # Pane #1
        self.pane_game = ttk.Frame(self.paned, height=100, width=200, padding=5)
        self.pane_game['borderwidth'] = 5
        self.pane_game['relief'] = 'sunken'
        self.pane_game.grid(row=1, column=1, padx=5, pady=5, sticky="nsew")

        self.paned.place(relx=0.5, rely=0.6, anchor="center")

        self.paned.add(self.pane_game, weight=1)

    def invite_player(self):
        self.master.clipboard_clear()
        self.master.clipboard_append(f"https://ppoker.com/play/{self.game.id}")
        self.invite_success.place(relx=0.88, rely=0.13, anchor="center")

        self.after(10000, self.hide_invite_success)

    def hide_invite_success(self):
        self.invite_success.place_forget()

    def new_game(self):
        self.parent.show_controller(self.parent.menu_controller)

    def close(self):
        self.parent.close_all()