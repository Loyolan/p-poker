class Backlog:
    def __init__(self, task, note):
        self.task = task
        self.note = note

    def __str__(self):
        return "{}/{}".format(self.task, self.note)