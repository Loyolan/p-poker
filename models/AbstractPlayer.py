from abc import ABC, abstractmethod


class AbstractPlayer(ABC):
    def __init__(self, name, url=None):
        self.name = name
        self.url = url
        self.hand = []

    @abstractmethod
    def show_hand(self):
        pass