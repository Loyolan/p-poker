# Players, backlog, current_ticket, current_player, rules and game_state
class Game:
    def __init__(self, id, owner=None, current_ticket=None, current_player=None, players=None, nb_players=2):
        self.id = id
        self.current_ticket = current_ticket
        self.current_player = current_player
        self.owner = owner
        self.nb_players = nb_players
        self.players = players

    def __str__(self):
        return "{}{}".format(self.name, self.id)